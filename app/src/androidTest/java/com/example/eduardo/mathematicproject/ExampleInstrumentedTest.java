package com.example.eduardo.mathematicproject;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.base.Strings;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static final String LOG_TAG = ExampleInstrumentedTest.class.getSimpleName();

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Calendar c = new GregorianCalendar();
        Date date = c.getTime();
        long milis = date.getTime();
        System.out.println(c.toString());
        Log.d(LOG_TAG, c.toString());
        System.out.println(milis);
        Log.d(LOG_TAG, String.valueOf(milis) );

        assertEquals("com.example.eduardo.mathematicproject", appContext.getPackageName());
    }
}
