package com.example.eduardo.mathematicproject.ws.dao;

import android.content.Context;
import android.util.Log;

import com.example.eduardo.mathematicproject.model.Place;
import com.example.eduardo.mathematicproject.model.RawResponse;
import com.example.eduardo.mathematicproject.ws.singleton.HttpClientSingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jcastro on 01/06/2017.
 */

public class PlacesRetHlp {

    private final String LOG_TAG = PlacesRetHlp.class.getSimpleName();

    public Place[] action(Context context, double lat, double lon, int radius){
        Place[] places;
        HttpClientSingleton client = HttpClientSingleton.getInstance();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://67.205.143.79:10000/Places").newBuilder();
        urlBuilder.addQueryParameter("lat", String.valueOf(lat));
        urlBuilder.addQueryParameter("lng", String.valueOf(lon));
        urlBuilder.addQueryParameter("radius", String.valueOf(radius));
        String url = urlBuilder.build().toString();
        System.out.println(url);

        Request request = new Request.Builder()
                .url(url)
                .build();

        Gson gson = new Gson();

        try{
            Response response = client.getClient().newCall(request).execute();
            //Log.d(LOG_TAG, "Response: \n" + response.toString());
            //System.out.println(response.toString());
            RawResponse rawResponse = gson.fromJson(response.body().charStream(), RawResponse.class);
            places = rawResponse.getData();
            response.close();
            return places;
        } catch (Exception e){
            //Log.e(LOG_TAG, e.getMessage());
            System.out.println(e.getMessage());
            return null;
        }
    }

    public Place[] action (Context contex, double lat, double lon, int radius, String type){
        Place[] places;
        HttpClientSingleton client = HttpClientSingleton.getInstance();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://67.205.143.79:10000/Places").newBuilder();
        urlBuilder.addQueryParameter("lat", String.valueOf(lat));
        urlBuilder.addQueryParameter("lng", String.valueOf(lon));
        urlBuilder.addQueryParameter("radius", String.valueOf(radius));
        urlBuilder.addQueryParameter("type", type);
        String url = urlBuilder.build().toString();
        System.out.println(url);

        Request request = new Request.Builder()
                .url(url)
                .build();

        Gson gson = new Gson();

        try{
            Response response = client.getClient().newCall(request).execute();
            //Log.d(LOG_TAG, "Response: \n" + response.toString());
            //System.out.println(response.toString());
            RawResponse rawResponse = gson.fromJson(response.body().charStream(), RawResponse.class);
            places = rawResponse.getData();
            response.close();
            return places;
        } catch (Exception e){
            //Log.e(LOG_TAG, e.getMessage());
            System.out.println(e.getMessage());
            return null;
        }
    }

}
