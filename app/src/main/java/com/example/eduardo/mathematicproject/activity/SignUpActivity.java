package com.example.eduardo.mathematicproject.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eduardo.mathematicproject.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private Context mContext = SignUpActivity.this;
    private EditText mEtName;
    private EditText mEtEmail;
    private EditText mEtPass;
    private EditText mEtRepeatPass;
    private Button mBtnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mEtName = (EditText) findViewById(R.id.login_input_name);
        mEtEmail = (EditText) findViewById(R.id.login_input_email);
        mEtPass = (EditText) findViewById(R.id.login_input_password);
        mEtRepeatPass = (EditText) findViewById(R.id.login_input_rep_password);
        mBtnSignUp = (Button) findViewById(R.id.btn_sign_up);
        mBtnSignUp.setOnClickListener(SignUpActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_sign_up:
                attemptSignUp();
        }
    }

    private void attemptSignUp(){
        if(validateForm()){
            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Registrando");
            progressDialog.show();
            ParseUser user = new ParseUser();
            user.setEmail(mEtEmail.getText().toString());
            user.setUsername(mEtName.getText().toString());
            user.setPassword(mEtPass.getText().toString());
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null){
                        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                }
            });
        }
    }

    private boolean validateForm(){
        String name = mEtName.getText().toString();
        String email = mEtEmail.getText().toString();
        String pass = mEtPass.getText().toString();
        String pass2 = mEtRepeatPass.getText().toString();

        if (name.isEmpty()){
            mEtName.setError("Campo obligatorio");
            return false;
        }
        else if(email.isEmpty()){
            mEtEmail.setError("Campo obligatorio");
            return false;
        }
        else if(pass.isEmpty()){
            mEtPass.setError("Campo obligatorio");
            return false;
        }
        else if(pass2.isEmpty()){
            mEtRepeatPass.setError("Campo obligatorio");
            return false;
        }

        if(!pass.equals(pass2)){
            mEtRepeatPass.setError("La contraseña no coincide");
            return false;
        }

        return true;
    }
}
