package com.example.eduardo.mathematicproject.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eduardo.mathematicproject.R;
import com.example.eduardo.mathematicproject.utils.ValidationUtils;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private final String LOG_TAG = LoginActivity.class.getSimpleName();

    private final Context mContext = LoginActivity.this;
    private AutoCompleteTextView mTvEmail;
    private EditText mTvPassword;
    private Button mBtnLogin;
    private Button mBtnSignUp;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ParseUser user = ParseUser.getCurrentUser();
        if(user != null){
            Intent intent = new Intent(mContext, MainActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_login);
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage(getString(R.string.login));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mTvEmail = (AutoCompleteTextView) findViewById(R.id.login_input_name);
        mTvEmail.setText("edu.castro.jecc@gmail.com");
        mTvPassword = (EditText) findViewById(R.id.login_input_password);
        mTvPassword.setText("myebbcc2089");
        mBtnLogin = (Button) findViewById(R.id.login_btn_sign_in);
        mBtnLogin.setOnClickListener(LoginActivity.this);
        mBtnSignUp = (Button) findViewById(R.id.login_btn_invite);
        mBtnSignUp.setOnClickListener(LoginActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btn_sign_in:
                //El usuario ha hecho click en el botón "INGRESAR"
                attemptLogin();
                break;
            case R.id.login_btn_invite:
                //El usuario ha hecho click en el botón "REGISTRARME"
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mTvEmail.setError(null);
        mTvPassword.setError(null);

        // Store values at the time of the login attempt.
        String mail = mTvEmail.getText().toString();
        String password = mTvPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !ValidationUtils.isPasswordValid(password)) {
            mTvPassword.setError(getString(R.string.error_invalid_password));
            focusView = mTvPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mail)) {
            mTvEmail.setError(getString(R.string.error_field_required));
            focusView = mTvEmail;
            cancel = true;
        } else if (!ValidationUtils.isEmailValid(mail)) {
            mTvEmail.setError(getString(R.string.error_invalid_email));
            focusView = mTvEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            // showProgress(true);

            mProgressDialog.show();
            logInWithEmail(mail, password);

        }
    }

    private void logInWithEmail(String mail, final String password){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("email", mail);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                if(e == null && list.size() == 1){
                    String name = list.get(0).getUsername();
                    logInWithName(name, password);
                }
                else if(list != null){
                    if(list.isEmpty()){
                        mProgressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, getString(R.string.error_not_registered), Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    mProgressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void logInWithName(final String userName, final String password){
        ParseUser.logInInBackground(userName, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                if(e == null){
                    //mSessionManager.createLoginSession(userName, password, parseUser.getObjectId(), parseUser.getObjectId());
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }
                else {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
