package com.example.eduardo.mathematicproject.model;

/**
 * Created by jcastro on 02/06/2017.
 */

public class RawResponse {
    private String msg;
    private Place[] data;
    private int status;
    private int count;

    public RawResponse(){

    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Place[] getData() {
        return data;
    }

    public void setData(Place[] data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
