package com.example.eduardo.mathematicproject.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.eduardo.mathematicproject.R;
import com.example.eduardo.mathematicproject.adapters.PlacesArrayAdapter;
import com.example.eduardo.mathematicproject.model.Place;
import com.example.eduardo.mathematicproject.utils.LocationUtils;
import com.example.eduardo.mathematicproject.ws.dao.PlacesRetHlp;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private final Context mContext = MainActivity.this;
    private final int ACCESS_FINE_LOCATION_PERMISSION_CODE = 0;

    private GoogleMap mMap;
    private Toolbar mToolbar;
    private ListView mBottomSheet;
    private BottomSheetBehavior mBsb;
    private CoordinatorLayout mCl;
    private LocationManager mLocationManager;
    private Location mLocation;
    private Marker mLocationMarker;
    private String mType;
    private Intent GoList;
    private Dialog mSearchDialog;
    private CoordinatorLayout mCoordinatorLayout;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLocation = location;
            updateMarker(mLocation.getLatitude(), mLocation.getLongitude());
            //updateCamera(mLocation.getLatitude(), mLocation.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinatorLayout);
        mToolbar = (Toolbar) findViewById(R.id.main_appbar);
        setSupportActionBar(mToolbar);
        try {
            getSupportActionBar().setTitle(getString(R.string.app_name));
            getSupportActionBar().setSubtitle(ParseUser.getCurrentUser().getUsername());
        } catch (Exception e){
            e.printStackTrace();
        }

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mCl = (CoordinatorLayout) findViewById(R.id.main_coordinatorLayout);
        findViewById(R.id.main_fab_search).setOnClickListener(this);
        mBottomSheet = (ListView) findViewById(R.id.main_lv_bottomSheet);
        mBsb = BottomSheetBehavior.from(mBottomSheet);
        mBsb.setState(BottomSheetBehavior.STATE_HIDDEN);
        createSearchDialog();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    this.ACCESS_FINE_LOCATION_PERMISSION_CODE);
        } else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
            //mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            //Solicitamos permisos al usuario para obtener ubicación
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MainActivity.this.ACCESS_FINE_LOCATION_PERMISSION_CODE);
        } else {
            //Los permisos ya estan concedidos, podemos proceder
            Location location = LocationUtils.getLastKnownLocation(mContext, mLocationManager, ACCESS_FINE_LOCATION_PERMISSION_CODE);
            if(location != null){
                mLocation = location;
                updateMarker(mLocation.getLatitude(), mLocation.getLongitude());
                //updateCamera(mLocation.getLatitude(), mLocation.getLongitude());
                updateCameraZoom(mLocation.getLatitude(), mLocation.getLongitude(), 18);
            }
        }
    }

    private void updateMarker(double lat, double lon){
        if(mLocationMarker != null)
            mLocationMarker.remove();
        if(mMap != null){
            mLocationMarker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(lat, lon))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person_pin_black_36dp))
                                    .title("Mi ubicación"));
        }
    }

    private void updateCamera(double lat, double lon){
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(lat, lon));
        mMap.moveCamera(center);
    }

    private void updateCameraZoom(double lat, double lon, int zoom){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), zoom);
        mMap.animateCamera(update);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_action_logout:
                showLogOutDialog();
                return true;
            default:
                return false;
        }
    }

    private void showLogOutDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.alert_logout_title))
                .setMessage(getString(R.string.alert_logout_message))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logOutUser();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private void createSearchDialog(){
        mSearchDialog = new Dialog(mContext);
        mSearchDialog.setContentView(R.layout.type_selection);
        mSearchDialog.setTitle(getString(R.string.alert_search_title));
        mSearchDialog.findViewById(R.id.alert_search_btn_atm).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_banks).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_bars).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_cafe).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_church).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_gas).setOnClickListener((View.OnClickListener) mContext);
        mSearchDialog.findViewById(R.id.alert_search_btn_restaurant).setOnClickListener((View.OnClickListener) mContext);
    }

    private void logOutUser(){
        final ProgressDialog pd = new ProgressDialog(mContext);
        pd.setMessage(getString(R.string.loging_out));
        pd.show();
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                pd.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_fab_search:
                //mBsb.setState(BottomSheetBehavior.STATE_EXPANDED);
                mType = null;
                mSearchDialog.show();
                break;
            case R.id.alert_search_btn_atm:
                //Snackbar.make(findViewById(R.id.main_coordinatorLayout), "Message", Snackbar.LENGTH_SHORT).show();
                mType = "atm";
                break;
            case R.id.alert_search_btn_banks:
                mType = "bank";
                break;
            case R.id.alert_search_btn_bars:
                mType = "bar";
                break;
            case R.id.alert_search_btn_cafe:
                mType = "cafe";
                break;
            case R.id.alert_search_btn_church:
                mType = "church";
                break;
            case R.id.alert_search_btn_gas:
                mType = "gas_station";
                break;
            case R.id.alert_search_btn_restaurant:
                mType = "restaurant";
                break;
        }
        if(mType != null){
            /*GoList = new Intent(getApplicationContext(), ListPlacesActivity.class);
            GoList.putExtra(getString(R.string.key_type), mType);
            startActivity(GoList);*/
            mSearchDialog.dismiss();
            GetPlacesTask getPlacesTask = new GetPlacesTask(mType);
            getPlacesTask.execute();
        }


    }

    private void showSnackBar(String message, int duration){
        Snackbar snackbar = Snackbar.make(mCl, message, duration);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case ACCESS_FINE_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
                        //mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
                    }
                }
                else{
                    Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private class GetPlacesTask extends AsyncTask<Void, Void, Void>{
        PlacesRetHlp retHlp;
        List<Place> listplaces;
        List<Place> places;
        ProgressDialog progressDialog;
        String type;

        public GetPlacesTask(String type){
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            retHlp = new PlacesRetHlp();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setTitle("Buscando lugares");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            places = new ArrayList<>();
            Place[] place;
            if(type != null)
                place = retHlp.action(getApplicationContext(), mLocation.getLatitude(), mLocation.getLongitude(), 500, type);
            else
                place = retHlp.action(getApplicationContext(), mLocation.getLatitude(), mLocation.getLongitude(), 500);

            if(place == null)
                return null;

            for ( Place item : place) {
                places.add(item);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            PlacesArrayAdapter adapter = new PlacesArrayAdapter(MainActivity.this, 0, places);
            mBottomSheet.setAdapter(adapter);
            if(mMap != null) {
                for (Place place : places) {
                    LatLng sydney = new LatLng(place.getLat(), place.getLng());
                    mMap.addMarker(new MarkerOptions().position(sydney).title(place.getName()));
                }
            }
            progressDialog.dismiss();
            if(places == null)
                Snackbar.make(mCoordinatorLayout,
                    "0 Lugares encontrados",
                    Snackbar.LENGTH_LONG).show();
            else {
                Snackbar.make(mCoordinatorLayout,
                        String.valueOf(places.size()) + " Lugares encontrados",
                        Snackbar.LENGTH_LONG).show();
                if(places.size() > 0)
                    mBsb.setState(BottomSheetBehavior.STATE_EXPANDED);
                else if(places.size() == 0)
                    mBsb.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        }
    }

}
