package com.example.eduardo.mathematicproject.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.eduardo.mathematicproject.R;
import com.example.eduardo.mathematicproject.model.Place;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by cbriceno on 08/06/2017.
 */

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {

    private List<Place> places;
    private Context context;
    private static final String Request_Url= "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";
    private static final String Key = "&key=AIzaSyAD0mAUNtwC7ihAullxyv6n6zAKPnBgybA";

    public PlacesAdapter(Context context,List<Place> places) {
        this.places = places;
        this.context = context;
    }

    private Context getContext(){
        return context;
    }

    @Override
    public PlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View placeView = inflater.inflate(R.layout.place_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(placeView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlacesAdapter.ViewHolder holder, int position) {

        Place place = places.get(position);

        TextView title = holder.title;
        title.setText(place.getName());

        TextView location = holder.location;
        location.setText(place.getVicinity());

        RatingBar rating = holder.rating;
        rating.setRating(place.getRating());

        ImageView img = holder.img;

        Picasso.with(getContext()).load(Request_Url + place.getPhoto_reference() + Key).placeholder(R.drawable.cast_album_art_placeholder).error(R.drawable.cast_ic_notification_disconnect).into(img);

    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView location;
        public RatingBar rating;
        public ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            location = (TextView) itemView.findViewById(R.id.location);
            img = (ImageView) itemView.findViewById(R.id.img_place);
            rating = (RatingBar) itemView.findViewById(R.id.rating);
        }
    }
}
