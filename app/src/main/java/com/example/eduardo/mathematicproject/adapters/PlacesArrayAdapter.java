package com.example.eduardo.mathematicproject.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.eduardo.mathematicproject.R;
import com.example.eduardo.mathematicproject.model.Place;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jcastro on 10/06/2017.
 */

public class PlacesArrayAdapter extends ArrayAdapter<Place> {
    private List<Place> mPlaces;
    private static final String Request_Url= "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";
    private static final String Key = "&key=AIzaSyAD0mAUNtwC7ihAullxyv6n6zAKPnBgybA";

    public PlacesArrayAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public PlacesArrayAdapter(Context context, int resource, List<Place> places){
        super(context, resource, places);
        mPlaces = places;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if(view == null){
            LayoutInflater inflater;
            inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.place_item, null);
        }

        Place item = getItem(position);

        if(item != null){
            CircleImageView image = (CircleImageView) view.findViewById(R.id.img_place);
            TextView title = (TextView) view.findViewById(R.id.title);
            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

            if(item.getPhoto_reference() != null)
                Picasso.with(getContext()).load(Request_Url + item.getPhoto_reference() + Key).placeholder(R.drawable.cast_album_art_placeholder).error(R.drawable.cast_ic_notification_disconnect).into(image);
            if(item.getName() != null)
                title.setText(item.getName());

            ratingBar.setRating(item.getRating());
        }

        return view;
    }
}
