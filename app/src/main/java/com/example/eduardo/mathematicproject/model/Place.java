package com.example.eduardo.mathematicproject.model;

/**
 * Created by jcastro on 01/06/2017.
 */

public class Place {
    private double lat;
    private double lng;
    private float rating;
    private String vicinity;
    private String photo_reference;
    private String name;
    private String types;

    public Place(){

    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public float getRating() {
        return rating;
    }

    public String getVicinity() {
        return vicinity;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public String getName() {
        return name;
    }

    public String getTypes() {
        return types;
    }
}
