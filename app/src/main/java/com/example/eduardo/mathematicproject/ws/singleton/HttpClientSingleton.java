package com.example.eduardo.mathematicproject.ws.singleton;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by jcastro on 01/06/2017.
 */

public class HttpClientSingleton {
    private static HttpClientSingleton ourInstance = null;

    private OkHttpClient client;

    public static HttpClientSingleton getInstance() {
        if(ourInstance == null)
            ourInstance = new HttpClientSingleton();
        return ourInstance;
    }

    private HttpClientSingleton() {
        client = new OkHttpClient.Builder()
                .connectTimeout(500, TimeUnit.SECONDS)
                .build();
    }

    public OkHttpClient getClient(){
        return client;
    }
}
