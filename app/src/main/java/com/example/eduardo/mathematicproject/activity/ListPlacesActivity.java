package com.example.eduardo.mathematicproject.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.eduardo.mathematicproject.R;
import com.example.eduardo.mathematicproject.adapters.PlacesAdapter;
import com.example.eduardo.mathematicproject.adapters.PlacesArrayAdapter;
import com.example.eduardo.mathematicproject.model.Place;
import com.example.eduardo.mathematicproject.ws.dao.PlacesRetHlp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cbriceno on 08/06/2017.
 */

public class ListPlacesActivity extends AppCompatActivity {

    private RecyclerView recycler_places;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listplaces);

        Intent intent = getIntent();
        Toast.makeText(getApplicationContext(),intent.getStringExtra(getString(R.string.key_type)), Toast.LENGTH_LONG).show();
        recycler_places = (RecyclerView) findViewById(R.id.listplaces);



        Background background = new Background();
        background.execute();



    }


    public class Background extends AsyncTask<Void,Void,Void>{
        PlacesRetHlp retHlp;
        List<Place> listplaces;
        List<Place> places;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            retHlp = new PlacesRetHlp();


        }

        @Override
        protected Void doInBackground(Void... params) {
           places = new ArrayList<>();
            Place[] place = retHlp.action(getApplicationContext(), 21.021188, -89.626842, 500);

            for ( Place item : place
                 ) {
                System.out.println(item.getRating());
                places.add(item);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            PlacesAdapter adapter = new PlacesAdapter(getApplicationContext(), places);

            recycler_places.setAdapter(adapter);
            recycler_places.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }
    }
}
