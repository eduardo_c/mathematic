package com.example.eduardo.mathematicproject.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parse.Parse;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            Parse.initialize(new Parse.Configuration.Builder(this)
                    .applicationId("KRMud9JuFFbCjIqgsF9YqAXkVsgk3ZWBld0SDFGa")
                    .clientKey("FZZNSW1tkXDW1vteXeSqJ978ac9RWBLgTu7EfniR")
                    .server("https://parseapi.back4app.com/").build()
            );
        }
        catch (Exception e){
            e.printStackTrace();
        }

        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }
}
