package com.example.eduardo.mathematicproject.utils;

import android.content.Context;

import com.example.eduardo.mathematicproject.R;
import com.parse.ParseException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by eduardo on 25/04/17.
 */

public class ValidationUtils {
    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with other constraint
        return password.length() > 4;
    }

    public static String getErrorCause(Context context, int code){
        switch (code){
            case ParseException.OTHER_CAUSE:
                return context.getString(R.string.error_server);
            case ParseException.CONNECTION_FAILED:
                return context.getString(R.string.error_connection);
            case ParseException.OBJECT_NOT_FOUND:
                return context.getString(R.string.error_invalid_login_parameters);
            case ParseException.INVALID_QUERY:
                return context.getString(R.string.error_invalid_query);
            case ParseException.INVALID_CLASS_NAME:
                return context.getString(R.string.error_invalid_class_name);
            case ParseException.MISSING_OBJECT_ID:
                return context.getString(R.string.error_missing_object_id);
            case ParseException.INVALID_KEY_NAME:
                return context.getString(R.string.error_invalid_key_name);
            case ParseException.INVALID_POINTER:
                return context.getString(R.string.error_invalid_key_name);
            case ParseException.INVALID_JSON:
                return context.getString(R.string.error_invalid_json);
            case ParseException.COMMAND_UNAVAILABLE:
                return context.getString(R.string.error_command_unavailable);
            case ParseException.NOT_INITIALIZED:
                return context.getString(R.string.error_not_initialize);
            case ParseException.INCORRECT_TYPE:
                return context.getString(R.string.error_incorrect_type);
            case ParseException.INVALID_CHANNEL_NAME:
                return context.getString(R.string.error_invalid_channel_name);
            case ParseException.PUSH_MISCONFIGURED:
                return context.getString(R.string.error_push_misconfigured);
            case ParseException.TIMEOUT:
                return context.getString(R.string.error_timeout);
            case ParseException.INVALID_EMAIL_ADDRESS:
                return context.getString(R.string.error_invalid_email_address);
            case ParseException.USERNAME_MISSING:
                return context.getString(R.string.error_username_missing);
            case ParseException.PASSWORD_MISSING:
                return context.getString(R.string.error_password_missing);
            case ParseException.USERNAME_TAKEN:
                return context.getString(R.string.error_username_taken);
            case ParseException.EMAIL_TAKEN:
                return context.getString(R.string.error_email_taken);
            case ParseException.EMAIL_MISSING:
                return context.getString(R.string.error_email_missing);
            case ParseException.EMAIL_NOT_FOUND:
                return context.getString(R.string.error_email_not_found);
            case ParseException.INVALID_SESSION_TOKEN:
                return context.getString(R.string.error_invalid_session_token);
            default:
                return context.getString(R.string.error_code) + code;
        }
    }
}
