package com.example.eduardo.mathematicproject;

import com.example.eduardo.mathematicproject.ws.dao.PlacesRetHlp;
import com.example.eduardo.mathematicproject.ws.singleton.HttpClientSingleton;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        PlacesRetHlp retHlp = new PlacesRetHlp();
        System.out.println(retHlp.action(null, 21.021188, -89.626842, 500).length);
        assertEquals(4, 2 + 2);
    }
}